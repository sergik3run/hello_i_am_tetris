﻿using System;
using System.Linq;

namespace assets.scripts
{
    public enum FigureType
    {
        S = 0,
        L,
        O,
        T,
        J,
        Z,
        I
    }

    public static class FigureGenerator
    {
        private static readonly FigureType[] _figureTypes = Enum.GetValues(typeof(FigureType)).Cast<FigureType>().ToArray();

        public static int[,] GetRandomFigure()
        {
            var index = UnityEngine.Random.Range(0, _figureTypes.Length);
            var figureType = _figureTypes[index];
            var result = new int[2, 2];
            switch (figureType)
            {
                case FigureType.S:
                    result = _sBlock;
                    break;
                case FigureType.L:
                    result = _lBlock;
                    break;
                case FigureType.O:
                    result = _oBlock;
                    break;
                case FigureType.T:
                    result = _tBlock;
                    break;
                case FigureType.J:
                    result = _jBlock;
                    break;
                case FigureType.Z:
                    result = _zBlock;
                    break;
                case FigureType.I:
                    result = _iBlock;
                    break;
            }

            return result;
        }

        private static int[,] _tBlock =
        {
            { 1,1,1 },
            { 0,1,0 }
        };

        private static int[,] _oBlock =
        {
            {1,1 },
            {1,1 }
        };

        private static int[,] _iBlock =
        {
            {1,1,1,1}
        };

        private static int[,] _sBlock =
        {
            {0,1,1 },
            {1,1,0 }
        };

        private static int[,] _zBlock =
        {
            {1,1,0 },
            {0,1,1 }
        };

        private static int[,] _jBlock =
        {
            {1,1,1},
            {0,0,1}
        };

        private static int[,] _lBlock =
        {
            {1,1,1},
            {1,0,0}
        };
    }
}