﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisArray : MonoBehaviour
{
    int[,] field = new int[,]
    {
        {0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0},
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
        {0,0,0,0,0,0,0,0,0,0 },
    };

    int[,] _tBlock =
    {
        { 1,1,1 },
        { 0,1,0 }
    };

    int[,] _oBlock =
    {
        {1,1 },
        {1,1 }
    };

    int[,] _iBlock =
    {
        {1,1,1,1}
    };

    int[,] _sBlock =
    {
        {0,1,1 },
        {1,1,0 }
    };

    int[,] _zBlock =
    {
        {1,1,0 },
        {0,1,1 }
    };

    int[,] _jBlock =
    {
        {1,1,1},
        {0,0,1}
    };

    int[,] _lBlock =
    {
        {1,1,1},
        {1,0,0}
    };
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
