﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace assets.scripts
{
    public class GameField
    {
        private const int ROW = 20;
        private const int COLUMN = 10;

        private const int EMPTY_FIELD = 0;
        private const int FIGURE = 1;
        private const int STATIC = 2;

        private const int ROW_ADD_FIGURE_INDEX = 1;
        private const int COLUMN_ADD_FIGURE_INDEX = COLUMN / 2;

        private int[,] _field;

        public GameField()
        {
            _field = new int[ROW, COLUMN];
            ClearField();
        }

        public static GameField Create()
        {
            return new GameField();
        }
        
        public void DropDown()
        {
            for (int i = ROW-1; i >0 ; i--)
            {
                for (int j = 0; j < COLUMN; j++)
                {
                    //first case: figure drop down to bottom
                    if (i == ROW - 1 && _field[i, j] == FIGURE)
                    {
                        Replace();
                        return;
                    }

                    //second case: figure drop down to static
                    if (i + 1 < ROW && _field[i, j] == FIGURE && _field[i + 1, j] == STATIC)
                    {
                        Replace();
                        return;
                    }

                    //third case: figure drop down by one row
                    if(_field[i,j] == FIGURE)
                    {
                        var temp = _field[i, j];
                        _field[i, j] = _field[i + 1, j];
                        _field[i + 1, j] = temp;
                    }
                }

            }
        }

        public void AddFegure(int [,] figure)
        {
            var rowNumber = figure.GetLength(0);
            var columnNumber = figure.GetLength(1);
            for (int i = 0; i < rowNumber; i++)
            {
                for (int j = 0; j < columnNumber; j++)
                {
                    _field[ROW_ADD_FIGURE_INDEX + i, COLUMN_ADD_FIGURE_INDEX + j] = figure[i, j];
                    //Need add check overflow of game field

                }
            }
            
        }

        public void Rotate()
        {

        }

        public void Move()
        {

        }

        private void ClearField()
        {
            for(int i =0; i < ROW; i++)
            {
                for(int j = 0; j < COLUMN; j++)
                {
                    _field[i, j] = 0;
                }
            }
        }

        private void Replace()
        {
            for (int i = 0; i < ROW; i++)
            {
                for (int j = 0; j < COLUMN; j++)
                {
                    if (_field[i, j]== FIGURE)
                    {
                        _field[i, j] = STATIC;
                    }

                }

            }
        }

        public override string ToString()
        {
            var str = GetLog();
            return str;
        }

        private string GetLog()
        {
            var result = "";
            for (int i = 0; i < ROW; i++)
            {
                var row = "";
                for (int j = 0; j < COLUMN; j++)
                {
                    row += string.Format("{0} ", _field[i, j]);
                }
                row += "\n";
                result += row;
            }
           
            return result;
        }

    }
}